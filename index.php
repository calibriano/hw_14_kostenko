<?php
require_once 'products.php';

use \cart\Cart;
use \cart\exchange\Exchange;
//Авозагрузка классов

function __autoload($class){
    $path = str_replace('\\','/', $class).'.php';
    if(file_exists($path)){
        require $path;
    }
}

$product = new Cart();
$change = new Exchange();
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <div>
        <h2>Корзина:</h2>
        <?php if(!empty($product->getProducts())): ?>
        <?php foreach($product->getProducts() as $cartProd):?>
            <?=$cartProd->name ?> - <?=$change->convert($cartProd->price, 'usd') ?><br><br>
        <?php endforeach;?>
        <?php else: ?>
            <h4>Корзина пуста</h4>
        <?php endif; ?>
    </div>
    <h3>Товары</h3>
<?php foreach($products as $id => $product):?>
    <div style="float: left; margin: 15px;">
        <?=$product['name']?><br>
        Цена: <?=$change->convert($product['price'], 'uah')?><br>
        <a href="add2cart.php?id=<?=$id ?>" type="button">Купить Сейчас</a>
    </div>
<?php endforeach;?>
</body>
</html>
