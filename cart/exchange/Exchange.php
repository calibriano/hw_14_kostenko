<?php

namespace cart\exchange;

class Exchange
{
    protected $price;
    protected $curr;

 public  function convert ($price, $needCurr)
 {
     if($needCurr == 'usd'){
         $final = round($price / 28.1,1);
         return $final.' USD';
     }elseif ($needCurr == 'eur'){
         $final = round($price / 36.5,1);
         return $final.' EUR';
    }else{
         return $price.' UAH';
     }
 }
}