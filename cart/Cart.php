<?php
namespace cart;

class Cart
{
    protected $products;

    public function __construct()
    {
        if(isset($_COOKIE['cart'])){
            $this->products = json_decode($_COOKIE['cart']);
        }

    }

    public function getProducts(){
        return $this->products;
    }

    public function saveCart(){
        $value = json_encode($this->products);
        setcookie('cart', $value, time()+60, "/");
    }

    public function addProduct($prod){
        $this->products[] = $prod;
    }

}